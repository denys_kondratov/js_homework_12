import { pizzaSelectSize, pizzaSelectTopping, show, validate, pizzaSelectToppingDrag, random } from "./functions.js";
import { pizzaUser } from "./data-pizza.js"

//const [...inputs] = document.querySelectorAll("#pizza input");
/*
inputs.forEach((input)=>{
    input.addEventListener("click", ()=>{
        console.log("+")
    })
})
*/

document.querySelectorAll(".grid input")
    .forEach((input) => {
        if (input.type === "text" || input.type === "tel" || input.type === "email") {
            input.addEventListener("change", () => {
                if (input.type === "text" && validate(/^[А-я-іїґє]{2,}$/i, input.value)) {
                    selectInput(input, pizzaUser)
                } else if (input.type === "tel" && validate(/^\+380\d{9}$/, input.value)) {
                    selectInput(input, pizzaUser)
                } else if (input.type === "email" && validate(/^[a-z0-9_.]{3,}@[a-z0-9._]{2,}\.[a-z.]{2,9}$/i, input.value)) {
                    selectInput(input, pizzaUser)
                } else {
                    input.classList.add("error")
                }
            })
        } else if (input.type === "reset") {
            input.addEventListener("click", () => {

            })
        } else if (input.type === "button") {
            input.addEventListener("click", () => {   
                
                const userName = document.querySelector(".grid input[type='text']");
                const userTel = document.querySelector(".grid input[type='tel']");
                const userMail = document.querySelector(".grid input[type='email']");

                if (userName.value !== "" && userTel.value !== "" && userMail.value !== "" && pizzaUser.sauce !== "" && pizzaUser.topping.length) {
                    localStorage.userInfo = JSON.stringify(pizzaUser);
                    window.open('./thank-you/index.html', '_self');
                } else if (userName.value === "") {
                    userName.classList.add("error");
                } else if (userTel.value === "") {
                    userTel.classList.add("error");
                } else if (userMail.value === "") {
                    userMail.classList.add("error");
                }
            })
        }
    });

function selectInput(input, data) {
    input.className = "";
    input.classList.add("success");
    data.userName = input.value;
};

document.querySelector("#pizza")
    .addEventListener("click", pizzaSelectSize);

document.querySelector(".ingridients")
    .addEventListener("click", pizzaSelectTopping);

// DragAndDrop

let dragSousAll = document.querySelector(".ingridients");

let dropCrust = document.getElementById("dropCrust");

dragSousAll.addEventListener('dragstart', function (evt) {
    let elem = evt.target;    

    elem.style.border = "3px dotted #000";
    
    evt.dataTransfer.effectAllowed = "move";
    
    evt.dataTransfer.setData("Text", elem.id);
}, false);

// конец операции drag
dragSousAll.addEventListener("dragend", function (evt) {
    evt.target.style.border = ""; 
}, false);

// перетаскиваемый объект попадает в область целевого элемента
dropCrust.addEventListener("dragenter", function (evt) {
    this.style.border = "3px solid red";
}, false);

// перетаскиваемый элемент покидает область целевого элемента
dropCrust.addEventListener("dragleave", function (evt) {
    this.style.border = "";
}, false);

dropCrust.addEventListener("dragover", function (evt) {    
    if (evt.preventDefault) evt.preventDefault();
    return false;
}, false);

// перетаскиваемый элемент отпущен над целевым элементом
dropCrust.addEventListener("drop", function (evt) {
    if (evt.preventDefault) evt.preventDefault();
    if (evt.stopPropagation) evt.stopPropagation();

    this.style.border = "";

    let id = evt.dataTransfer.getData("Text");    
    let elem = document.getElementById(id);    

    pizzaSelectToppingDrag(elem); 
    
    return false;
}, false);

show(pizzaUser);

const banner = document.querySelector("#banner");

banner.addEventListener('mouseenter', () => {
    banner.style.right = `${random(0, 70)}%`;
    banner.style.bottom = `${random(0, 70)}%`;
});